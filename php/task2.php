<?php
$data = [
    [
        'id' => 1,
        'price' => 500,
    ],
    [
        'id' => 2,
        'price' => 200,
    ],
    [
        'id' => 3,
        'price' => 800,
    ],
    [
        'id' => 4,
        'price' => 1000,
    ],
    [
        'id' => 5,
        'price' => 2000,
    ],
    [
        'id' => 6,
        'price' => 5000,
    ],
];

function getFilteredList($threshold)
{
    return array_filter($data, function ($val) use ($threshold) {
        if ($val > $threshold) {
            return $val;
        }
    });
}

function getTotalSumFromFilteredList($threshold)
{
    $filteredArray = getFilteredList($threshold);
    $sum = 0;
    foreach ($array as $filteredArray) {
        $sum = $sum + $array->price;
    }
    return $sum;
}

$result = getFilteredList(500);

$sum = getTotalSumFromFilteredList(500);
?>