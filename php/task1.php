<?php
$url = 'http://ftp.geoinfo.msl.mt.gov/Documents/Metadata/GIS_Inventory/35524afc-669b-4614-9f44-43506ae21a1d.xml';

function fetchRequest($url){
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL,$url);
  curl_setopt($curl, CURLOPT_FAILONERROR,1);
  curl_setopt($curl, CURLOPT_FOLLOWLOCATION,1);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
  curl_setopt($curl, CURLOPT_TIMEOUT, 15);
  $responseValue = curl_exec($curl);          
  curl_close($curl);
  return $responseValue;
}

$rawData = fetchRequest($url);
$xmlData = new SimpleXMLElement($rawData);
$jsonData = json_encode($xmlData);
print_r($jsonData)
?>