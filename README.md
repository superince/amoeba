Find below the Questions:
 JavaScript
Use any JavaScript design pattern to create a simple dom based library(similar to Jquery) which will be able to perform following tasks:
a.       Change the class name of any dom elements
b.       Get datasets from dom elements
c.       Inject new dom element
d.       Make both ajax and get request
e.       Get and set values from input box/checkbox/select dropdown
2.              Make 3 POST requests at the same time which need to be resolved all at the same time. 


 PHP
Use this link to get XML data and convert those data in more readable JSON format
We have an array of IDs and prices. Please create two functions as per the below, 
One which will return the lists, which have prices greater than a certain threshold that can be dynamic provided by the user. 
Another function will return the total sum of prices from that filtered list
