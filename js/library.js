const Library = (() => {
  const getElement = (selector) => {
    return document.querySelector(selector);
  };

  const getElementWithId = (id) => {
    return document.getElementById(id);
  };

  return {
    changeClass: (selector, newClassName) => {
      const element = getElement(selector);
      if (element) {
        element.className = newClassName;
      }
    },
    getDataset: (elementId) => {
      const element = getElementWithId(elementId);
      if (element) {
        return element.dataset;
      }
    },
    injectNewDom: (containerId, newElement) => {
      const targetContainer = getElementWithId(containerId);
      if (!targetContainer) {
        console.error("Target Container not found");
        return;
      }
      targetContainer.appendChild(newElement);
    },
    makeAjaxRequest: (url, method, data) => {
      return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();

        xhr.onreadystatechange = () => {
          if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
              return resolve(xhr.responseText);
            } else {
              return reject(`Request Failed with status: ${xhr.status}`);
            }
          }
        };

        xhr.open(method, url, true);
        xhr.send(data);
      });
    },
    makeGetRequest: (url, method, data, headers) => {
      return fetch(url, {
        method: method,
        headers: headers,
        body: JSON.stringify(data),
      });
    },
    getValues: (elementId) => {
      const targetElement = getElementWithId(elementId);
      if (!targetElement) {
        console.error("Target Element not found");
        return;
      }
      const values = [...targetElement.options].map((option) => option.value);
      return values;
    },
    setValue: (elementId, value) => {
      const targetElement = getElementWithId(elementId);
      if (!targetElement) {
        console.error("Target Element not found");
        return;
      }
      targetElement.options[targetElement.options.length] = new Option(value,value);
    },
  };
})();
