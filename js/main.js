// Changing class name
Library.changeClass("h1", "helloworld");

// Get datasets
const dataset = Library.getDataset("myDiv");
console.log(dataset);

// Add new element
const newElement = document.createElement("p");
const node = document.createTextNode("I am new p element");
newElement.appendChild(node);
Library.injectNewDom("container", newElement);

// Ajax Request
const url = "https://jsonplaceholder.typicode.com/todos/1";
Library.makeAjaxRequest(url, "GET")
  .then((result) => {
    console.log(result);
  })
  .catch((e) => {
    console.error("error", e);
  });

// Get Request
Library.makeGetRequest(url, "GET")
  .then((result) => {
    return result.json();
  })
  .then((result) => {
    console.log(result);
  })
  .catch((e) => {
    console.error("error", e);
  });

// Get values from input
const values = Library.getValues("jobPost");
console.log(values);

// Set values to input
Library.setValue("jobPost", "Full stack developer");

// 3 post request at the same time
const uri = "https://jsonplaceholder.typicode.com/todos";
const request1 = fetch(`${uri}/1`);
const request2 = fetch(`${uri}/2`);
const request3 = fetch(`${uri}/3`);

Promise.all([request1, request2, request3])
  .then((response) => {
    response.map((res) => {
      res.json().then((jsonRes) => {
        console.log(jsonRes);
      });
    });
  })
  .catch((e) => {
    console.error(e);
  });
